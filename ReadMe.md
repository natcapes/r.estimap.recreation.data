About
-----

This repository hosts a sample GRASS GIS data base
and a GRASS GIS independent example data set
to accompany the `r.estimap.recreation` GRASS GIS add-on.
The data base contains a minimal set of input maps (projected in EPSG:3035)
to practice the examples demonstrated at
[gitlab.com/natcapes/r.estimap.recreation](https://gitlab.com/natcapes/r.estimap.recreation).
The example data set (either of the provided .zip or .tar.gz files),
can be used to practice with the (currently experimental) QGIS plugin
[ESTIMAP Recreation](https://planet.qgis.org/plugins/natcapes_qgis-master/).
Note that the QGIS plugin
is only a front-end to the GRASS GIS addon
[gitlab.com/natcapes/r.estimap.recreation](https://gitlab.com/natcapes/r.estimap.recreation)
that exposes the algorithm under the Processing Toolbox.

## Content

Map names are self-explanatory. However, a few more words on what is what exactly.
All maps depict features and locations over our area of interest.


### Raster maps

<p float="center">
    <img src="images/r_estimap_recreation_input_area_of_interest.png" alt="Area of interest" width="180" />
    <img src="images/r_estimap_recreation_input_regions.png" alt="Administrative regions" width="180" />
</p>

<p float="center">
    <img src="images/r_estimap_recreation_input_corine_land_cover_2006.png" alt="CORINE land cover 2006" width="180" />
    <img src="images/r_estimap_recreation_input_land_suitability.png" alt="Land suitability" width="180" />
    <img src="images/r_estimap_recreation_input_land_use.png" alt="Land use map" width="180" />
</p>

<p float="center">
    <img src="images/r_estimap_recreation_input_water_resources.png" alt="Water resources" width="180" />
    <img src="images/r_estimap_recreation_input_bathing_water_quality.png" alt="Bathing water quality" width="180" />
    <img src="images/r_estimap_recreation_input_urban_green.png" alt="Urban green" width="180" />
    <img src="images/r_estimap_recreation_input_protected_areas.png" alt="Natural protected areas" width="180" />
</p>

<p float="center">
    <img src="images/r_estimap_recreation_input_distance_to_infrastructure.png" alt="Distance to infrastructure" width="180" />
    <img src="images/r_estimap_recreation_input_local_administrative_units.png" alt="Administrative units" width="180" />
    <img src="images/r_estimap_recreation_input_population_2015.png" alt="Population density 2015" width="180" />
</p>


- `input_area_of_interest` defines the area of interest, somewhere in Austria
- `input_bathing_water_quality` depicts locations of bathing waters
- `input_corine_land_cover_2006` is a fragment from the well known CORINE land data base
- `input_distance_to_infrastructure` scories the available infrastructure via which to reach locations of recreation activities
- `input_land_suitability` scores the access to and the suitability of existing land resources for recreation
- `input_land_use` shows land features from which to derive suitability for recreation. Such a map can be used with a series of scores (one score for each land feature category) to derive a **land suitability** map.
- `input_local_administrative_units` delineates the administrative areas for which to compute statistics required by the mobility function.
- `input_population_2015` is a map of population density (for the year 2015)
- `input_protected_areas` is the fragment from the [World Database of Protected Areas](https://www.iucn.org/theme/protected-areas/our-work/world-database-protected-areas)
- `input_regions` groups smaller local administrative units in larger entities
- `input_urban_green` depicts green cover in urban surfaces
- `input_water_resources` depicts water components

### Vector maps

Currently only one vector map is provided (in form of a GeoPackage file):

- `input_vector_local_administrative_units.gpkg` is the actual source of the
`input_local_administrative_units` raster map

### Categories, rules and more

GRASS GIS features elaborated mechanisms
to assign colors, labels or descriptions
to the categories of a raster map.
Further,
using a simple syntax and logic,
in form of plain text files,
rules can be build to explain
how one raster map's categories
can be reclassified to a new scheme.

The directory `categories_and_rules`
contains various such plain text files
that were useful while developing
the `r.estimap.recreation` addon.
The one file that is, at least, required,
in order to go through the examples,
is `rules_corine_to_maes_land_classes.txt`.
